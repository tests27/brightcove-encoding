export enum BrightcoveStreamingProtocol {
  RTMP = "rtmp", // FLV
  RTP = "rtp", // MPEG2-TS
  RTP_FEC = "rtp-fec", // MPEG2-TS
  SRT = "srt", // MPEG2-TS
}

export enum BrightcoveRegion {
  EU_CENTRAL_1 = "eu-central-1",
  EU_WEST_1 = "eu-west-1",
  US_WEST_2 = "us-west-2",
  US_EAST_1 = "us-east-1",
  AP_SOUTH_1 = "ap-south-1",
  AP_SOUTHEAST_1 = "ap-southeast-1",
  AP_SOUTHEAST_2 = "ap-southeast-2",
  AP_NORTHEAST_1 = "ap-northeast-1",
}

export interface BrightcoveCdn {
  label: string;
  prepend: string;
  protocol: "http" | "https";
  token_auth?: {};
  vendor?: string;
}

export interface BrightcoveAlternateAudio {
  tracks: Array<{
    language: string;
    video_pid: number;
    pid: number;
    default: boolean;
    label?: string;
    variant?: "main" | "alternate" | "commentary" | "supplementary" | "dub";
    streams?: Array<{}>; // TODO
  }>;
}

export type BrightcoveNotification =
  | string
  | {
      credentials?: string;
      event:
        | "state_changed"
        | "first_segment_uploaded"
        | "output_finished"
        | "rtmp_output_state_changed";
      url: string;
    };

export interface BrightcoveOutput {
  label: string;
  ad_audio_loudness_level?: number;
  audio_bitrate?: number;
  audio_codec?: "aac";
  color_full_width?: boolean;
  copy_audio?: boolean;
  copy_video?: boolean;
  credentials?: string;
  drm?: {
    models?: Array<"all" | "fairplay" | "widevine">;
    token_expires_in?: number;
  };
  h264_profile?: "baseline" | "main" | "high";
  height?: number;
  width?: number;
  keyframe_interval?: number;
  playlist_label?: string;
  mode?: "instant";
  live_stream: boolean;
  notifications?: Array<{
    event: "state_changed" | "first_segment_uploaded" | "output_finished";
  }>;
  remove_ads?: boolean;
  rendition_label?: string;
  segment_seconds: number;
  skip_audio?: boolean;
  skip_video?: boolean;
  streams?: [{ source: "1080p" }, { source: "720p" }]; // TODO
  type?: "playlist";
  url?: boolean;
  video_bitrate: number;
  video_codec?: "h264";
  videocloud?: {}; // TODO
}

interface BrightcoveCreateLiveJobAbstract {
  live_stream: true;

  ad_audio_loudness_level?: number;
  ad_insertion?: boolean;
  add_cdns?: Array<BrightcoveCdn>;
  alternate_audio?: BrightcoveAlternateAudio;
  audio_only?: boolean;
  audio_only_storage_format?: "mpeg-ts" | "aac";
  beacon_set?: string;
  channel_type?: "event" | "24x7";
  cidr_whitelist?: string[];
  ip_whitelist?: string[];
  copy_outputs_from_job?: string;
  drm?: {
    modes?: Array<
      | "all"
      | "fairplay"
      | "playready"
      | "widevine"
      | "widevine:dash"
      | "widevine:hls"
    >;
    token_expires_in?: number;
    require_playback_token?: boolean;
    check_playback_rights?: boolean;
  };
  encryption?: {
    check_playback_rights?: boolean;
    external_url?: string;
    key?: string;
    key_rotation?: boolean;
    method: "aes-128";
    passphrase?: string;
    rotate_every?: number;
    type: "internal" | "external";
  };
  event_length?: number;
  hls_endlist?: boolean;
  live_dvr_ads_window_duration?: number;
  live_dvr_sliding_window_duration?: number;
  live_sliding_window_duration?: number;
  max_hls_protocol_version?: number;
  notifications?: Array<BrightcoveNotification>;
  outputs: Array<BrightcoveOutput>;
  protocol?: BrightcoveStreamingProtocol;
  reconnect_time?: number;
  region: BrightcoveRegion;
  credentials?: string;
  rtmp_ip_whitelist?: Array<string>;
  slate?: string;
  srt_config?: {
    latency?: number;
  };
  static?: boolean;
  videocloud?: {
    account_id?: string;
    credentials?: string;
    live_to_vod?: boolean;
    mode?: "instant";
    video: {};
  };
  low_latency?: boolean;
  pass_through?: {
    dvr: false;
    stream_to_social_media: false;
  };
}

export interface BrightcoveCreateLiveJobWithCidrWhitelist
  extends BrightcoveCreateLiveJobAbstract {
  protocol:
    | BrightcoveStreamingProtocol.SRT
    | BrightcoveStreamingProtocol.RTP
    | BrightcoveStreamingProtocol.RTP_FEC;
  cidr_whitelist: string[];
}

export type BrightcoveCreateLiveJobInput =
  | BrightcoveCreateLiveJobWithCidrWhitelist
  | (BrightcoveCreateLiveJobAbstract & {
      protocol: BrightcoveStreamingProtocol.RTMP;
    });

export interface BrightcoveCreateLiveJobResponse {
  id: string;
  live_stream: boolean;
  stream_name: string;
  stream_url: string;
  ad_insertion: boolean;
  playback_added_cdns?: BrightcoveCdn;
  drm?: {
    token: string;
    mode:
      | "all"
      | "fairplay"
      | "playready"
      | "widevine"
      | "widevine:dash"
      | "widevine:hls";
  };
  event_length: number;
  max_hls_protocol_version: number;
  notifications: Array<BrightcoveNotification>;
  outputs: Array<{
    id: string;
    playback_url: string;
    playback_url_dvr: string;
    playback_url_vod: string;
  }>;
  reconnect_time: number;
  region: BrightcoveRegion;
  sep_state:
    | "waiting"
    | "pending_activation"
    | "activation_in_progress"
    | "ready"
    | "pending_deactivation"
    | "deactivation_in_progress"
    | "cancelled"
    | "finished";
  slate: string;
}
