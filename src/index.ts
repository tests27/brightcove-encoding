import { config as configDotenv } from "dotenv";
import * as path from "path";
import express from "express";
import LiveService from "./live.service";

configDotenv({
  path: path.resolve(__dirname, "..", ".env"),
});

const app = express();
app.use(express.json());

app.post("/start-live", async (req, res) => {
  try {
    const startLiveRes = await LiveService.StartLiveEncoding();

    return res.status(201).json(startLiveRes);
  } catch (err) {
    console.log(err);

    return res.sendStatus(500);
  }
});

app.get("/live", async (req, res) => {
  try {
    const liveEncodings = await LiveService.ListLiveEncodings();

    return res.status(200).json(liveEncodings);
  } catch (err) {
    console.log(err);

    return res.sendStatus(500);
  }
});

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => console.log(`Listening http://localhost:${PORT}`));
