import BrightCoveApi from "./brightcove.api";
import {
  BrightcoveRegion,
  BrightcoveStreamingProtocol,
} from "./brightcove.models";

const LiveService = {
  StartLiveEncoding() {
    return BrightCoveApi.CreateLiveJob({
      live_stream: true,
      region: BrightcoveRegion.EU_WEST_1,
      reconnect_time: 300,
      outputs: [
        {
          label: "hls720p",
          live_stream: true,
          height: 720,
          video_bitrate: 800,
          segment_seconds: 4,
          keyframe_interval: 90,
        },
      ],
      protocol: BrightcoveStreamingProtocol.RTMP,
    });
  },

  ListLiveEncodings() {
    return BrightCoveApi.ListLiveJobs();
  },
};

export default LiveService;
