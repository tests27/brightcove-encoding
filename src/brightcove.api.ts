import axios from "axios";
import {
  BrightcoveCreateLiveJobInput,
  BrightcoveCreateLiveJobResponse,
} from "./brightcove.models";

const BrightCoveApi = {
  CreateLiveJob(input: BrightcoveCreateLiveJobInput) {
    const url = "https://api.bcovlive.io/v1/jobs";

    return axios
      .post<BrightcoveCreateLiveJobResponse>(url, input, {
        headers: {
          "X-API-KEY": process.env.BRIGHTCOVE_API_KEY,
        },
      })
      .then(({ data }) => data);
  },

  ListLiveJobs() {
    const url = "https://api.bcovlive.io/v1/jobs?sort=created_at&sort_dir=desc";

    return axios
      .get<BrightcoveCreateLiveJobResponse>(url, {
        headers: {
          "X-API-KEY": process.env.BRIGHTCOVE_API_KEY,
        },
      })
      .then(({ data }) => data);
  },
};

export default BrightCoveApi;
