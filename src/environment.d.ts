declare global {
  namespace NodeJS {
    interface ProcessEnv {
      BRIGHTCOVE_BASE_URL: string;
      BRIGHTCOVE_API_KEY: string;
      S3_OUTPUT_BUCKET_NAME: string;
      S3_OUTPUT_ACCESS_KEY: string;
      S3_OUTPUT_SECRET_KEY: string;
    }
  }
}

export {};
